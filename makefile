.DELETE_ON_ERROR:

# On créé des variables contenant la liste des fichiers qui nous intéressent
# Dans sources des sh à transformer en html
# Dans annexfiles le reste des fichiers
sources    != find src -type f -and -name '*.sh'
annexfiles != find ui -type f
resourcesfiles != find src/*/resources -type d

# On construit dynamiquement les règles à invoquer avec des substitutions de
# chaînes de caractères
# Ex: Pour pages on prend tous les chemins de fichiers récupérés dans sources
#     On substitue src/ par public/ et l'extension md ou sh par html
#     Le fichier source "src/truc/bidule.sh" donnera donc
#     "public/truc/bidule.html"
# Même mécanique pour les raw et les fichiers annexes

htmltargets    != echo "$(sources)" | sed -E 's/src/public/g;s/\.(md|sh)/.html/g'
annextargets   = $(annexfiles:ui/%=public/ui/%)
resourcestargets  != echo "$(resourcesfiles)" | sed -E 's/src/public/g'

# On appelle toutes les cibles pour produire tous les fichiers
# nécessaires
# Pour chacune make va tenter de trouver une règle correspondante dans la liste
# qui suit
all: exec ${htmltargets} ${resourcestargets} ${annextargets}

# Règle permettant de vider root si besoin
# Faire make clean pour l'appeler
clean:;
	rm -r public/*

# Règle pour générer le css
public/style.css: ui/style.css; cp $< $@

# Règle pour générer le favicon
public/favicon.png: ui/favicon.png; cp $< $@

# Syntaxe générale d'une règle :
# cible : liste dépendances ; commandes
#
# Règles pour générer les fichiers
# Chaque cible requiert des dépendances après le ":"
# Si l'un des ses dépendances est plus récent que la cible
# alors la cible est reconstruite
# Ce que % match dans la cible sera substitué à la place de % dans les
# dépendances
# make substitue la variable $@ avec le chemin de la cible et $< avec le
# chemin de la première dépendance de la règle

# Règle pour la génération des pages html
public/%.html: src/%.sh page common
	mkdir -p $(shell dirname $@); $< > $@

# Pour les autres fichiers
public/%: src/%
	mkdir -p $(shell dirname $@); cp -r $< $@

public/ui/%: ui/%
	mkdir -p $(shell dirname $@); cp $< $@

page: layout/html ; touch $@

common: layout/html ; touch $@

exec:;
	chmod +x src/*.sh