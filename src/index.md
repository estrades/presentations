- _[Présentation générale de la plateforme - état 2024](estrades.html)_, 2024.

- _[Réflexions et retours d’expérience sur l’intégration de l’ATR dans le cadre la plateforme d’appui à la recherche Estrades](ATR-Nancy-10-09-2024.html)_, Titouan Brisset Saboureau, Guillaume Porte et Elsa Van Kote, journée _De la transcription manuelle participative des textes à la reconnaissance automatique de texte (ATR/HTR): outils, théorie, pratiques_, 10 septembre 2024.

