## Estrades : présentation

- plateforme d'appui à la recherche qui propose des "Solutions de TRanscription, d'Analyse et de Diffusion pour l'Edition Structurée".

- portée par l'ARCHE UMR 3400 (Unistra) et la MISHA, avec le soutien de la BNU et du ILLE (UHA)

- menée par un petit groupe d'ingénieur.e.s de l'Unistra (labos, DNUM, SBU), de la MISHA et de la BNU

- débutée en 2020, labellisée par Cortecs en 2023, prix Idex en 2024, candidature en cours auprès du RnMSH (Scripto)

- **[estrades.huma-num.fr](https://estrades.huma-num.fr)**






