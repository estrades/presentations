## Cisame : objectifs et défis

1. **Exploration des interactions entre les savoirs au XII^e siècle**  
   - Étude des relations entre différents domaines de connaissance tels que le droit, la théologie, et la philosophie.

2. **Analyse d'un large corpus de manuscrits**  
   - Objectif de fournir une vue d'ensemble des interactions entre ces savoirs avant leur formalisation en disciplines distinctes.

3. **Problèmes rencontrés**  
   - Création de modèles : développement de chaînes de traitement complexes, difficiles à maîtriser et à transposer d'un projet à un autre.

