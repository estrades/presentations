## Cisame : un besoin ATR partagé

1. **Réflexion déjà en cours Estrades/BNU** 
  - Collaboration et partage de ressources pour des besoins communs en reconnaissance de texte manuscrit
  - Evaluation de la solution FONDUE en 2022 non retenue pour des questions RH
  - Première instance escriptorium (2023 - VM 8 vcpu / 256Go de stockage / 32Go de RAM)

2. **Enseignement et formations**  
  - Utilisation d'escriptorium dans le cadre de la formation initiale : faculté des sciences historiques (master d'études médiévales interdisciplinaire) et faculté des langues
  
3. **Autres projets de chercheurs et chercheuses**  
  - Besoin d'ATR pour divers projets académiques (herbier de l'Institut de Botanique... )
