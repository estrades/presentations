##  ATR : Kraken

 - Installation d'une instance Kraken sur le centre de calcul de l'Unistra par la DNUM (pôle CESAR - David Brusson), offrant une grande puissance de calcul.

 - Connexion via SSH.

 - Jusqu'à présent, les besoins en ATR n'ont pas nécessité de coûts supplémentaires pour l'utilisation du centre de calcul. Les demandes sont traitées en fonction de la disponibilité du centre de calcul (autrement appels à projets DNUM).

 - Une documentation est en cours de rédaction pour les utilisateurs et utilisatrices.