## ATR : transformation en TEI et édition numérique

1. **Création de feuilles XSLT PAGE vers TEI**  

   - Interopérabilité possible grâce à l’utilisation de l’ontologie Segmonto.

2. **Retour à la chaîne éditoriale**  

   - Format TEI et indexation dans BaseX pour raccrocher à la chaîne éditoriale 
   - Exploration des résultats via le moteur d'affichage MaX (PDN Caen).



