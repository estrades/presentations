# Réflexions et retours d’expérience sur l’intégration de l’ATR dans le cadre la plateforme d’appui à la recherche Estrades

- Titouan Brisset Saboureau (projet CISAME - Unistra)
- Guillaume Porte (ARCHE UMR 3400 - Unistra)
- Elsa Van Kote (PHuN - MISHA)

--- 

Nancy, 10 septembre 2024

---

![idex](commun/resources/logo-idex.png)