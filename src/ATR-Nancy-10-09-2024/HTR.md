## ATR : escriptorium

- Instance maintenue par la DNUM (pôle CESAR - Virgile Jarrige) et administrée par le Lab de la BNU (Madeleine Hubert, Elisa Michelet) - [https://escriptorium.unistra.fr/](https://escriptorium.unistra.fr/)
  
- **Limites :** plateforme principalement utilisée pour des besoins simples et de la formation (pas d'entraînement de modèles ou de fine-tuning à ce stade)

- **Perspectives** : des solutions plus complètes, comme celles proposées à Genève ou à l'INRIA, ne sont pas encore exclues si les besoins et les moyens grandissent.

