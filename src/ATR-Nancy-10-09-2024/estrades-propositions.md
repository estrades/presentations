## Estrades : propositions

- proposer une suite d'outils pour augmenter la maintenabilité et proposer une base technique commune

- utiliser au maximum les outils existants^**\*** et concentrer les développements sur les connecteurs^**\*\***

- identifier les personnes au niveau local pour travailler en réseau &amp; inclure au maximum les personnels précaires dans les dynamiques collectives

- _tenter_ aussi de mutualiser une partie des moyens des projets 

---

- **\*** GitLab, escriptorium, MaX... et bientôt TACTEO ?
- **\*\*** Heimdall



