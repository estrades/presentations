## Constat et questions

- fort intérêt pour les interfaces

- au contraire, utilisation assez faible de kraken en dehors de CISAME et de la BNU

- allouer plus de puissance a escriptorium ? (maintenance et administration)

- orienter vers d'autres solutions similaires (instance Inria) ou privées (Calfa, Transkribus, Teklia... question des coûts et de la réutilisabilité des modèles)
 
- développer la formation sur kraken / améliorer les scripts PAGE/ALTO -> TEI

