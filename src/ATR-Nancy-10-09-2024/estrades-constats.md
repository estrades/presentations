## Estrades : constats

- importance du principe de chaîne éditoriale

- beaucoup d'étapes = compétences nécessaires nombreuses

- temps projet et financements consacrés à des développements secondaires plutôt qu'à la recherche

- multiplication des outils, des solutions, des systèmes, des codes sources = problèmes de maintenance / de maîtrise / de sobriété

- personnels titulaires débordés / personnels précaires souvent isolés et/ou aux 18 casquettes

- des institutions/services plus concurrents que complémentaires

