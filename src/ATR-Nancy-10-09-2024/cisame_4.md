## Cisame : état actuel 

1. **Développement en cours de l'ATR à Strasboug** 
 - projet qui a permis de mettre en place le moteur kraken au centre de calcul de l'Unistra et de développer l'utilisation de l'instance escriptorium.

2. **Réflexion juridique**
 - création d'une charte avec la BNU sur des questions juridiques pour l'utilisation d'éditions récentes pas encore libres de droit.

3. **Une insertion en cours dans la chaîne de traitement**
 - intérêt du projet pour le reste de la chaîne notamment la partie édition numérique associée à des bases de données relationnelles.
