## Cisame : la question de l'ATR

1. **Création de modèles via eScriptorium** (avril 2023)
   - Tentatives sur les serveurs de l'INRIA : défis techniques et temps d'attente prolongés.

2. **Utilisation de Kraken** (juin 2023)
   - Sur ordinateur personnel pour un usage local.

3. **Implémentation de Kraken sur le centre de calcul de l’Université de Strasbourg** (juillet 2023)  
   - Réalisée avec l'aide de la PHuN et du pôle César pour bénéficier de la puissance de calcul nécessaire.

