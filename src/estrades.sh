#! /usr/bin/env ./page
title: DHNord2024
author: Guillaume Porte
pageType: slider

slides:

../commun/intro-generique.md
../commun/estrades-description.md
../commun/estrades-constats-chaine.md     ../commun/estrades-constats-chaine-img.md
../commun/estrades-constats-tapor-img.md
../commun/estrades-constats-chaine.md     ../commun/estrades-constats-chaine-img.md
../commun/estrades-constats-problemes.md
../commun/estrades-propositions1.md
../commun/estrades-propositions2.md
../commun/estrades-propositions3.md    ../commun/estrades-propositions-outils-img.md
../commun/estrades-propositions4.md
../commun/estrades-propositions5.md
../commun/estrades-forces-faiblesses.md
../commun/conclusion-generique.md

endofslides
