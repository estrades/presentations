```
<<@@ cat
<section class="slide">
	<div class="flex">'
		<div  class="col-50 container">
			$(show slide1)
		</div>
		<div  class="col-50 container">
			$(show slide2)
		</div>
		<span class="licence">CC BY</span>
	</div>
</section>
@@
```