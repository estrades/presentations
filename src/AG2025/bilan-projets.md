## Projets 2024

- Flohr - le voyage en Amérique (Isabelle Laboulais, SAGE)
- Gregorius Digital (Peter Andersen, ARCHE)
- Chiknowpo (Marie Bizais Lillig, GEO)
- Carnets de fouille de Mouriès (Loup Bernard, ARCHIMEDE)

<br/>

- Textes judiciaires de l’époque moderne (Antoine Follain, ARCHE)
- Livres municipaux du Rhin Supérieur (Olivier Richard, ARCHE/Université de Fribourg)
- Les bibliothèques françoises de La Croix du Maine et de Du Verdier (Toshinori Uetani, BVH CESR Tours)
- Vocabulaire pour l’Étude des Scripturalités (Thomas Brunner, ARCHE)

<br/>

- QURABIB (Emily Cottrell/Eric Vallet, Institut d'islamologie)
- Le Recueil des rymes et proses de E. P. (1555) (Michela Lagnena, UHA)
- Les Diverses Leçons d’Antoine du Verdier (1577-1592) (Romane Marlhoux, UHA)
- PROC (Anne Réach Ngô/Michela Lagnena/Romane Marloux/Marine Parra, ILLE/MISHA)