**Dépenses**

|utilisation|année|montant|
|---|---|---|---|
|Formation à l'édition numérique (repas 2 jours 26  personnes)|2024|676€|
|Formation à l'édition numérique (pauses 3 jours - boulangerie)|2024|90,26€|
|Formation à l'édition numérique (pauses 3 jours - auchan)|2024|40,69€|
|A/R réunion des plateformes du RnMSH (1 personne)|2024|188€|
|---|---|---|---|
||**total**|**999,95€**|
