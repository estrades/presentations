## Activités

- Co-travail de l'équipe tous les derniers jeudis du mois au Lab (10h-12h discussions, 14h-17h ateliers)
  - rencontre avec le PDN/CERTIC (Caen) autour de MaX v2 (février 2024) 
  - formation mutuelle à des outils d'édition numérique (et autres)
  - stratégie d'organisation entre ingénieur.e.s du site alsacien
  - retours d'expérience

- Co-travail toutes les deux semaines Phun/Estrades (surtout Régis et Guillaume)

- Participation au GT3 du consortium ARIANE (ateliers "scripts" et "chaînes éditoriales") (Marine P., Guillaume P.)

- Participation aux ateliers Scripto du RnMSH

- Maintenance et administration de la plateforme eScriptorium Unistra (Virgile J., Elisa M.)

- Formations à eScriptorium (Elisa M. au Lab et Titouan B.-S. au MEMI)

- Rapprochements avec le consortium MASA (projet ARCHEAN)
