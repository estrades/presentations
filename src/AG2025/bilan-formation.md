## Formations

**« Formation à l'édition numérique de corpus », 2e édition**

- 24-26 juin 2024
- Lab de la BNU
- 15 participant.e.s
  - locaux et de l'intérieur
    - doctorant.e.s, ingénieur.e.s et EC

<br/>

**Ateliers PROC**

- organisé avec le Lab de la BNU
- ateliers référencement et TEI / Wikidata




