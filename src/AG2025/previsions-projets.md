## Projets 2025

**prioritaires (publication web)**

- Gregorius Digital (Peter Andersen, ARCHE)
- Chiknowpo (Marie Bizais Lillig, GEO)

**à venir / en cours**

- QURABIB (Emily Cottrell/Eric Vallet, Institut d'islamologie)
- Écriture et mémoire de la bataille de Badr (Adrien de Jarmy/Renaud Soler/Membourou Moimecheme Clarck Junior/Eric Vallet, Institut d'islamologie)
- Carnets de fouille de Mouriès (Loup Bernard, ARCHIMEDE)
- Le Trésor des bons esprits français (1602) (Anne Réach-Ngô, ILLE UHA)
- Projet Ritter+ (BNU)

**TEI to PDF**

- Le Recueil des rymes et proses de E. P. (1555) (Michela Lagnena, UHA)
- Les Diverses Leçons d’Antoine du Verdier (1577-1592) (Romane Marlhoux, UHA)

**maintenance et mises à jours**

- Textes judiciaires de l’époque moderne (Antoine Follain, ARCHE)
- Livres municipaux du Rhin Supérieur (Olivier Richard, ARCHE/Université de Fribourg)
- Les bibliothèques françoises de La Croix du Maine et de Du Verdier (Toshinori Uetani, BVH CESR Tours)

