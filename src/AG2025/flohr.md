## Flohr - le voyage en Amérique (septembre 2024)

Mise en ligne de l'édition numérique pour une exposition à la médiathèque André Malraux

<iframe
  width="800"
  height="600"
  src="https://estrades.huma-num.fr/flohr-expo/fr/edition/fr/de-page8.html">
</iframe>

