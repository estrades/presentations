**Dépenses**

|utilisation|montant|
|---|---|
|Assemblée générale (traiteur)|341,99€|
|Matériel informatique divers|242,45€|
|**sous total 1 (engagé)**|**584,44€**|
|---|---|---|---|
|Formation à l'édition numérique|900€|
|Missions|1500€|
|Stage|2500€|
|Prestation|4000€|
|Vacation Gregorius|4500€|
|Journées Scripto|?|
|**sous total 2 (estimation)**|**13&#160;400€**|
|---|---|---|---|
||**total**|**13&#160;984,44€**|