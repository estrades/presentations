## Equipe/groupe de travail

- Mohammed Benkhalid (UMR 7044 Archimède)
- Titouan Brisset-Saboureau (UMR 7354 DRESS)
- Arthur Brody (BNU)
- David Brusson (DNUM)
- Virgile Jarrige (DNUM)
- Elisa Michelet (Lab - BNU)
- Etienne Nadji (L’Ouvroir – MISHA)
- Guillaume Porte (UUMR 3400 ARCHE)
- Rosanne Wingert (BNU)
- Régis Witz (PHuN – MISHA)

**arrivées 2024**

- Marine Parra (BNU) [avril 2024]
- Amélie Quilichini (UHA) [octobre 2024]

