## Activités 2025

- réflexion sur les partenariats (BNU, UHA, DNUM)
- poursuite des réunions mensuelles de l'équipe
- participation aux ateliers ARIANE
- participation au(x) groupe(s) de travail MaX v2

<br/>

- 30 janvier : atelier TEI (Lab)
- 25-27 juin : formation à l'édition numérique, 3e édition (Lab)

<br/>

**envisagées**

- stage refonte XSLT
- prestation charte graphique
- travail sur le design web (avec fac des arts)
- accueil des journées Scripto

