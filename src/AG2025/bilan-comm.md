## Communications

- Elsa Van Kote, « Structurer des textes anciens en XML-TEI : une porte ouverte sur une culture numérique en Science humaines et sociales », _Techdays #15_, Strasbourg, 4 juin 2024.
- Titouan Brisset-Saboureau, « eScriptorium, une instance de transcription automatique mais pas seulement », _Techdays #15_, Strasbourg, 4 juin 2024.

**Présentations de la plateforme**

- _Journée ATR_, MSH Lorraine, Nancy, 10/09/2024 (Titouan B.-S. et Guillaume P.)
- _Table ronde du CARRA_, Strasbourg, 06/11/2024 (Guillaume P.)
- _DH Nord 2024_, Lille, 08/11/2024 (Guillaume P.)
- _Journées Cortecs 2024_, Strasbourg, 22/11/2024 (Régis W.)
- _Séance poster Humanités numériques en pédagogie et en recherche 2_, Faculté des langues, Strasbourg, 10/09/2024 (Guillaume P.)





