## Budget 2025

**Crédits**

|source|montant|
|---|---|---|---|---|
|Reste 2024|9&#160;000,05€|
|Cortecs 2025|5&#160;000€|
|Gregorius digital|4&#160;500€|
|---|---|---|
|**total**|**18&#160;500,05€**|

<br/>
<br/>

- le reliquat de 2024 (Cortecs + IdEx) doit être entièrement utilisé en 2025
- financement Gregorius fléché principalement vers ce projet (vacation TEI)
- autres financements envisagés de la part de projets en cours de montage ou non encore validés



