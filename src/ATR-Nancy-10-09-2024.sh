#! /usr/bin/env ./page
title: ATR-Nancy-10-09-2024
author: Titouan Brisset-Saboureau et Guillaume Porte
pageType: slider


slides:

intro.md
estrades.md
estrades-constats.md    estrades-constats-img.md
estrades-propositions.md    estrades-propositions-img.md
cisame_1.md    cisame_1_image.md
cisame_2.md    cisame_2_image.md
cisame_3.md
cisame_4.md
HTR.md    HTR_image.md
HTR_2.md    HTR_2_image.md
TEI_XSLT.md    estrades-propositions-img.md
Futur_estrade_1.md
../commun/conclusion-generique.md

endofslides