#! /usr/bin/env ./page
title: AG 2025 
author: Guillaume Porte
pageType: slider

slides:

index.md
bilan-2024.md
cortecs-2023.md
scripto-2024.md
prix-idex.md
flohr.md
refonte-site.md
bilan-projets.md
bilan-activites.md      bilan-activites-gitlab.md
bilan-equipe.md         bilan-equipe-2.md
bilan-comm.md
bilan-formation.md      images-formation.md
bilan-finances.md       bilan-depenses.md
previsions-2025.md
previsions-finances.md  previsions-depenses-2.md
previsions-projets.md
previsions-activites-2025.md
logo-1.md               logo-2.md
fin.md

endofslides