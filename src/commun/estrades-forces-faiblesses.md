## Forces et faiblesses

- modèle horizontal et volontaire enthousiasmant / _existe sur le temps "libre" et grâce à la bonne volonté de chacun.e_

- collectif fondé et mené par des ingénieurs / _question des fiches de postes et de la reconnaissance institutionnelle_

- comment passer d'une somme d'individus à une plateforme ? Le faut-il ?

- meilleure vision d'ensemble et plus grande capacité à envisager des solutions génériques / _difficulté à fédérer les moyens des projets financés pour permettre cette mutualisation_

- modèle idéal autour de l'_open-source_ et du monde Linux / _adaptation nécessaire (logiciels, OS, langages)_

- réflexion collective à des solutions pour une informatique plus sobre / _confrontation aux habitudes, aux compétences et aux attentes_



