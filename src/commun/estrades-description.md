## Description

- (projet de) plateforme d'appui à la recherche qui propose des _Solutions de TRanscription, d'Analyse et de Diffusion pour l'Edition Structurée_

- portée par l'ARCHE UMR&#160;3400 (Unistra) et la MISHA, avec le soutien de la BNU et du ILLE UR&#160;4363 (UHA)

- origines en 2019/2020, (véritablement) lancée fin 2022, labellisée par Cortecs en 2023, prix Idex en 2024, candidature en cours auprès du RnMSH (réseau Scripto &#129310;)

- s'appuie sur un groupe de travail composé d'ingénieur.e.s de l'Unistra (labos, DNUM, SBU), de la MISHA, de la BNU et de l'UHA

- liens avec réseaux nationaux (RnMSH, consortium Huma-Num)

- objectif principal : monter une plateforme d'accompagnement pour l'édition numérique entre les principales institutions et services actifs dans le domaine sur le site alsacien (formation, développement, PGD, science ouverte...)






