## Constats

- problèmes de maintenance / maîtrise des productions

- temps projet et financements consacrés à des développements secondaires plutôt qu'à la recherche disciplinaire

- personnels précaires souvent isolés et/ou aux 18 casquettes

- personnels titulaires souvent débordés ou gestionnaires de cahiers des charges

- difficultés à répondre à la demande, notamment pour les projets non financés

- (certain.e.s) chercheur.euse.s paumé.e.s face aux possibilités

- concurrence plus que complémentarité entre services

- une informatique pas hyper sobre