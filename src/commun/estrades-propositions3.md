## Propositions

- identifier les personnes au niveau local pour travailler en réseau &amp; inclure au maximum les personnels précaires dans les dynamiques collectives

- insister sur les formats et les vocabulaires plus que sur les outils 

- **proposer (quand même) une suite d'outils pour augmenter la maintenabilité et proposer une base technique commune :**
  
  - utiliser au maximum les solutions réutilisables
  - concentrer les développements sur les connecteurs pour tendre vers une chaîne idéale relativement générique
  - mutualiser au maximum les solutions techniques au niveau de la plateforme pour l'ensemble des projets accompagnés
  - aller vers des solutions techniquement plus sobres lorsque c'est possible (sites statiques etc.)

---

  - eScriptorium / kraken
  - [Tacteo](https://tacteo.huma-num.fr/)
  - BaseX / [MaX](https://mrsh.unicaen.fr/max/) (MRSH/Certic)
  - [Heimdall](https://gitlab.huma-num.fr/datasphere/heimdall) (MISHA/Phun)
  - [Catium](http://catium.bebou.netlib.re/)
  - gitlab





