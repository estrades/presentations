## Constats

- besoins convergents autour de la notion de chaîne éditoriale
 (chercheur.euse.s, services) mais pas de solution locale

- étapes et compétences nécessaires nombreuses

- multiplicité des outils, des solutions, des systèmes, des codes sources (éparpillement technologique)

- besoins souvents décrits comme spécifiques par méconnaissance ou par absence de vision plus globale

- interlocuteur.ice.s (trop) nombreux et peu ou pas coordonné.e.s
  
  - plusieurs services existants, peu dotés en ressources humaines et avec des compétences réparties selon des logiques autres

  - beaucoup de collègues en contrats courts, sur projet

- formation insuffisante