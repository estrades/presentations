## Propositions

- identifier les personnes au niveau local pour travailler en réseau &amp; inclure au maximum les personnels précaires dans les dynamiques collectives

- insister sur les formats et les vocabulaires plus que sur les outils 

- proposer (quand même) une suite d'outils pour augmenter la maintenabilité et proposer une base technique commune en cherchant à :
  
  - utiliser au maximum les solutions réutilisables, mutualiser les solutions et concentrer les développements sur les connecteurs pour tendre vers une chaîne idéale relativement générique  
  - aller vers des solutions techniquement plus sobres lorsque c'est possible

- améliorer la formation pour augmenter l'autonomie des chercheur.euse.s

- **_tenter_ de mutualiser une partie des moyens des projets financés (!)**