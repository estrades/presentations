## Propositions

- identifier les personnes au niveau local pour travailler en réseau &amp; inclure au maximum les personnels précaires dans les dynamiques collectives

- **insister sur les formats et les vocabulaires plus que sur les outils**
  
  - XML, HTML, MD, TXT, CSV... / TEI, EAD, IIIF...
  - tenter de faire sans interfaces de saisies (temps de développement + opacité)

