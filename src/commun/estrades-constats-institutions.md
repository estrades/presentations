## Estrades : constats

- chaîne éditoriale > étapes et compétences nécessaires nombreuses

- multiplication des outils, des solutions, des systèmes, des codes sources

  - problèmes de maintenance / de maîtrise / de sobriété

  - temps projet et financements consacrés à des développements secondaires plutôt qu'à la recherche

  - personnels titulaires débordés / personnels précaires souvent isolés et/ou aux 18 casquettes

- des institutions/services plus concurrents que complémentaires

