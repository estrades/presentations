window.onload = function() {
  /* au chargement, on rend visible la première slide */
  document.getElementsByClassName("slide")[0].classList.add("visible");
  /* au chargement on affiche la flèche de droite */
  arrows();
  /* au chargement, on ajuste le texte */
  textFit();
};


window.onresize = function() {
  /* au redimensionnement, on remet le font-size à sa valeur par défaut */
  var body = document.getElementsByTagName("body")[0];
  body.style.fontSize = "var(--space-lg)";
  /* au redimensionnement on affiche la flèche de droite */
  arrows();
  /* au redimensionnement, on ajuste le texte */
  textFit();
};

/* Lors d'un appui sur le clavier (flèche droite ou gauche),
et si une slide correspondante existe, on masque la slide en cours
et on affiche la slide demandée.
A chaque fois, on recalcule la taille du texte. */
document.onkeydown = function(event) {
  changeSlide(event);
};

/***** CHANGEMENT DES SLIDES *****/

/* Cette fonction réagit à deux événements : 
- un appui sur le clavier (fleche droite ou gauche)
- un click sur le bouton de navigation (dans le HTML, avec onclick="") */
function changeSlide(event){

  /* selection des slides en cours, précédente et suivante*/
  elem = document.getElementsByClassName("visible")[0];
  next = elem.nextElementSibling;
  prev = elem.previousElementSibling;
  arrowR = elem.getElementsByClassName("nav-right")[0];
  arrowL = elem.getElementsByClassName("nav-left")[0]

  /* on affiche la suivante ou la précédente en fonction :
  - de l'événement click ou keyCode
  - de l'existance ou non d'une slide précédente ou suivante */
    if(event.keyCode || event.type == "click"){
      switch (event.keyCode || event.target.parentNode.classList[0]) {
        case 37:
          if(prev){
            updateSlide(elem, prev)
          } else {}
        break;
        case 39:
          if(next){
            updateSlide(elem, next)
          } else {}
        break;
        case "nav-left":
          if(prev){
            updateSlide(elem, prev)
          } else {}
        break;
        case "nav-right":
          if(next){
            updateSlide(elem, next)
          } else {}
        break;
      }
    } 
}

/* on met à jour la slide courante
- ajustment du texte
- affichage des boutons de navigation */
function updateSlide(elem1, elem2){
  elem2.classList.add("visible");
  elem2.classList.remove("hidden");
  elem1.classList.add("hidden");
  elem1.classList.remove("visible");
  textFit();
  arrows();
}


/***** AFFICHAGE DES BOUTONS DE NAVIGATION *****/


/* affiche les barres de navigation à gauche et/ou droite selon 
l'existence d'une slide précédente ou suivante */
function arrows(){
  elem = document.getElementsByClassName("visible")[0];
  next = elem.nextElementSibling;
  prev = elem.previousElementSibling;
  arrowR = elem.getElementsByClassName("nav-right")[0];
  arrowL = elem.getElementsByClassName("nav-left")[0];

  if(prev){
    arrowL.style.display="block";
  } else{}

  if(next){
    arrowR.style.display="block";
  } else{}
}



/***** AJUSTEMENT DU TEXTE *****/

/* fonction d'appel */
function textFit(){
  /* on reset la font-size */
  var body = document.getElementsByTagName("body")[0];
  body.style.fontSize = "var(--space-lg)";
  getSlide();
}

/* sélectionne la slide en cours d'affichage */
function getSlide(){
  var containersList = new Array();
  var sections = document.getElementsByTagName("section");
  var body = document.getElementsByTagName("body")[0];
  for (let i = 0; i < sections.length; i++) {
    if(sections.item(i).classList.contains("visible")){
      //console.log(sections.item(i));
      var containers = sections.item(i).childNodes[1].childNodes;
      for (let a = 0; a < containers.length; a++) {    
        if(containers.item(a).classList && containers.item(a).classList.contains("container")){
          containersList.push(containers.item(a));
        }
      }
    }
  }
  for (let i = 0; i < containersList.length; i++) {
    adjustText(containersList[i], body);
  }
};


/* La taille de la fonte est calculée en fonction de la taille
de son conteneur afin que le texte ne déborde pas de l'écran.
On ne descend pas en dessous de 14px */
function adjustText(container, body){
  var wh = window.innerHeight; 
  var ch = container.offsetHeight;  
  //console.log(wh+" "+ch);
  var textSize = window.getComputedStyle(container, null).getPropertyValue('font-size');
  if(ch > wh && parseFloat(textSize) > 14){
    body.style.fontSize = parseFloat(textSize) - 1 + "px";
    adjustText(container, body);
  }
}

