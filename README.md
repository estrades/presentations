# Catium-slides

## Wàs ìsch / Qu'ès aquò ?

Catium slides permet de générer un fichier HTML qui se comporte comme un slider, à partir de fichiers markdown :
- on navigue de droite à gauche avec les flèches du clavier ou avec des boutons situés à gauche et à droite de l'écran
- le taille du texte s'adpate à la zone d'affichage
- chaque slide peut être en pleine page ou en deux colonnes

Il est basé sur [Catium](http://catium.bebou.netlib.re/) (ou plutôt sur une version antérieure de Catium, mais le principe reste le même).

## Comment ça marche ?

1. On crée un fichier `monSlider.sh` dans `./src`.

```
#! /usr/bin/env ./page
title: TITRE
author: AUTEUR
pageType: slider

slides:


endofslides
```

2. On crée un dossier `monSlider` dans `./src` qui contient nos markdown.

Si on veut utiliser des images non accessibles via URL, on crée un dossier `monSlider/resources` pour les y déposer.

3. Dans le fichier `monSlider.sh`, chaque slide est appelée sur une ligne.
- Si on appelle un seule slide, elle sera affichée en pleine page
- Si on appelle deux slides, elles occuperont chacune la moitié de l'écran

```
#! /usr/bin/env ./page
title: TITRE
author: AUTEUR
pageType: slider

slides:

slide1.md
slide2-1.md slide2-2.md
slide3.md

endofslides
```

4. dans le dossier racine, on lance un `make -B` qui génère un fichier `monSlider.html` dans `./public`.

## Comment l'utiliser ?

Forkez ou clonez ce dépôt et voyez plus haut **Comment ça marche ?**.


## Comment contribuer ?

La création des issues est possible à toute personne ayant un compte sur ce Gitlab.
Si vous voulez apporter des améliorations, envoyez moi un mail gporte@unistra.fr pour être ajouté au groupe.

---

## Explications

La première version de ce slider avait un fonctionnement assez lourd avec beaucoup de HTML pour gérer les colonnes dans les fichiers .sh, mais cela n'allait pas dans le sens de Catium qui gère cela dans `./layout`. Le fonctionnement actuel est une (nette) amélioration par Arthur Pons de ce premier prototype.

Restait le problème de la taille des conteneurs vs la taille de l'écran : un texte trop long débordait en bas et il fallait scroller pour voir la slide.

La solution retenue pour cette version est un script JS qui recalcule la taille du texte en fonction de la taille de l'écran. Cela se produit lors du **chargement** de la page, de son **redimensionnement**, et à chaque **changement de slide**.

Pour modifier les templates HTML, la transformation markdown, _etc._, il vaut mieux se repoter à [Catium](http://catium.bebou.netlib.re/) pour en apprendre plus sur le fonctionnement des fichiers `page`, `makefile` et des fichiers rangés dans `./layout`. 

## TODO

- [ ] Améliorer la partie web to print pour s'assurer que les slides tiennent bien sur une page A4.
- [ ] Simplifier `style.css`.

